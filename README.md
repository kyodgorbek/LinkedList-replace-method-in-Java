# LinkedList-replace-method-in-Java



import java.util.LinkedList;

public class LinkedListReplaceElementExample {

 public static void main(String[] args){
  
  //create LinkedList object
  LinkedList IList = new LinkedList();
  
  //add elements to LinkedList
  IList.add("1");
  IList.add("2");
  IList.add("3");
  IList.add("4");
  IList.add("5");
  
  System.out.println("LinkedList contains: " + IList);
  
  IList.set(3, "Replaced");
  System.out.println("After replacing 4, LinkedList contains: " +IList);
 }
}  
